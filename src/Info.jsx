import React from "react";

const Info = ({ name, address }) => {
  return (
    <div>
      <div>{name}</div>
      <div>{address}</div>
    </div>
  );
};

export default Info;
