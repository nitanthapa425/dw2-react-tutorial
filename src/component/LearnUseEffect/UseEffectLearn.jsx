import React, { useEffect, useState } from "react";

const UseEffectLearn = () => {
  console.log("A");

  let [count1, setCount1] = useState(0);
  let [count2, setCount2] = useState(10);
  let [count3, setCount3] = useState(11);

  let [documentTitle, setDocumentTitle] = useState("title1");
  let [documentTitle2, setDocumentTitle2] = useState("title1");

  useEffect(() => {
    document.title = documentTitle;

    let ram = count1 + 1;
    let shyam = count2 + 2;

    let cont33 = count3;
  }, []);

  //   useEffect(() => {
  //     console.log("i am  useEffect , i wll execut if count1 change");
  //   }, [count1]);

  return (
    <div>
      {console.log(" B")}
      count1 is {count1}
      <br></br>
      count2 is {count2}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Change count1
      </button>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        Change count2
      </button>
      <button
        onClick={() => {
          setCount3(count3 + 1);
        }}
      >
        Change count3
      </button>
      <button
        onClick={() => {
          setDocumentTitle("title2");
        }}
      >
        Change Title
      </button>
    </div>
  );
};

export default UseEffectLearn;
