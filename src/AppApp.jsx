import React from "react";
import LearnUseState2 from "./component/2LearnUseState";
import Check from "./component/Check";
import LearnForm from "./component/Form";
import HomeWork from "./component/HomeWork";
import UseEffectLearn from "./component/LearnUseEffect/UseEffectLearn";
import LearnUseState from "./component/LerntUseState";
import Form1 from "./component/LForm/Form1";
import P1 from "./component/ParentChidlBehaviour/P1";

const AppApp = () => {
  return (
    <div>
      {/* <LearnUseState></LearnUseState> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <P1></P1> */}

      {/* <Check></Check> */}
      {/* <UseEffectLearn></UseEffectLearn> */}
      {/* <HomeWork></HomeWork> */}
      {/* <LearnForm></LearnForm> */}
      <Form1></Form1>
    </div>
  );
};

export default AppApp;
