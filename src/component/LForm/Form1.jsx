import React, { useState } from "react";

//form elemnt
//input
//textarea
//select

//firsName//type="text"
//lastName//type= text
//email//type= "email"
//phoneNo//type="number"
//password//type="password"
//age//type="number"
//description=>textearea
//isMarried type="checkbox"
//country =>  select
//gender =>  radio box

//all form elment must have onChange and value prperty( for sate management)
//exept for the boxex input (eg radiobox and checkbox) that uses onChange and checked property of

//Those input which has option (eg select and  radio ) must have value property
// the value indicate value when specific option is clicked

//all have e.target.value expet for type="checkbox" which has e.target.value

const Form1 = () => {
  let [firstName, setFirstName] = useState("");
  let [lastName, setLastName] = useState("");
  let [password, setPassword] = useState("");
  let [email, setEmail] = useState(""); //aa
  let [phoneNo, setPhoneNo] = useState(0);
  let [description, setDescription] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [country, setCountry] = useState("");
  let [gender, setGender] = useState("");

  let handleSubmit = (e) => {
    e.preventDefault();
    let info = {
      _firstName: firstName,
      _lastName: lastName,
      _password: password,
      _email: email,
      _phoneNo: phoneNo,
      _description: description,
      _isMarried: isMarried,
      _country: country,
      _gender: gender,
    };

    console.log(info);
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="fistName">First Name</label>
        <input
          id="fistName"
          type="text"
          value={firstName}
          onChange={(e) => {
            setFirstName(e.target.value);
          }}
        ></input>
        <br></br>
        <br></br>
        <br></br>

        <label htmlFor="lastName">Last Name</label>
        <input
          id="lastName"
          type="text"
          value={lastName}
          onChange={(e) => {
            setLastName(e.target.value);
          }}
        ></input>

        <br></br>
        <br></br>
        <br></br>

        <label htmlFor="password">Password</label>
        <input
          id="password"
          type="password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        ></input>

        <label htmlFor="description">Description</label>
        <textarea
          id="description"
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
        ></textarea>

        <br></br>
        <br></br>
        <br></br>

        <label htmlFor="country">Country</label>
        <select
          id="country"
          value={country}
          onChange={(e) => {
            setCountry(e.target.value);
          }}
        >
          <option value="nep">Nepal</option>
          <option value="ind">India</option>
          <option value="chi">China</option>
          <option value="jap">Japan</option>
          <option value="kor">Korea</option>
        </select>

        <br></br>
        <br></br>
        <br></br>
        <br></br>

        {/* radio box */}

        <label htmlFor="male">Male</label>
        {/* gender="female" */}
        <input
          value="male"
          type="radio"
          id="male"
          checked={gender === "male"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <label htmlFor="female">Female</label>
        <input
          value="female"
          type="radio"
          id="female"
          checked={gender === "female"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <label htmlFor="other">Other</label>
        <input
          value="other"
          type="radio"
          id="other"
          checked={gender === "other"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <br></br>
        <br></br>
        <br></br>

        <label htmlFor="isMarried">Is Married</label>
        <input
          type="checkbox"
          id="isMarried"
          checked={isMarried}
          onChange={(e) => {
            setIsMarried(e.target.checked);
          }}
        ></input>

        <br></br>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form1;
