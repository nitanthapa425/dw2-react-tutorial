import React from "react";
import { matches } from "../data";

const Check = () => {
  // total players
  console.log(matches);

  let players = (match) => {
    let _players = match.map((value, i) => {
      let ary = [value.team1, value.team2];

      return ary;
    });

    return _players;
  };

  // unique total players
  let totalPlayers = players(matches);
  let uniquePlayers = totalPlayers.reduce((pre, current) => {
    let result = [...new Set([...pre, ...current])];
    return result;
  }, []);

  // console.log(uniquePlayers);

  //game played by each
  let playedGame = matches.filter((value, i) => {
    if (value?.score?.ft?.length) {
      return true;
    }
  });
  // console.log(playedGame);

  let playedPlayer = players(playedGame);

  // console.log(playedPlayer);

  let playedPlayerInArray = playedPlayer.reduce((pre, current) => {
    let result = [...pre, ...current];
    return result;
  }, []);
  // console.log(playedPlayerInArray);

  //calculating the playes with total game
  let calculateWithRepeatingNumber = (array) => {
    let _calculateWithRepeatingNumber = array.reduce((pre, current) => {
      if (pre[current]) {
        let result = { ...pre, [current]: pre[current] + 1 };
        return result;
      } else {
        let result = { ...pre, [current]: 1 };
        return result;
      }
    }, {});
    return _calculateWithRepeatingNumber;
  };

  let playerWithPlayedGame = calculateWithRepeatingNumber(playedPlayerInArray);

  // console.log(playerWithPlayedGame);

  //calculating the player with total win
  let calculateWithWinNumber = (array) => {
    let _calculateWithWinNumber = array.reduce((pre, current) => {
      if (current.score.ft[0] > current.score.ft[1]) {
        if (pre[current.team1] !== undefined) {
          let result = { ...pre, [current.team1]: pre[current.team1] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team1]: 1 };
          return result;
        }
      } else if (current.score.ft[1] > current.score.ft[0]) {
        if (pre[current.team2] !== undefined) {
          let result = { ...pre, [current.team2]: pre[current.team2] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team2]: 1 };
          return result;
        }
      }
      return pre;
    }, {});

    return _calculateWithWinNumber;
  };

  let winGame = calculateWithWinNumber(playedGame);

  console.log(winGame);

  //calculating with loss game
  let calculateWithLossNumber = (array) => {
    let _calculateWithLossNumber = array.reduce((pre, current) => {
      if (current.score.ft[0] > current.score.ft[1]) {
        if (pre[current.team2] !== undefined) {
          let result = { ...pre, [current.team2]: pre[current.team2] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team2]: 1 };
          return result;
        }
      } else if (current.score.ft[1] > current.score.ft[0]) {
        if (pre[current.team1] !== undefined) {
          let result = { ...pre, [current.team1]: pre[current.team1] + 1 };
          return result;
        } else {
          let result = { ...pre, [current.team1]: 1 };
          return result;
        }
      }
      return pre;
    }, {});

    return _calculateWithLossNumber;
  };

  let lossGame = calculateWithLossNumber(playedGame);

  console.log(lossGame);

  //calculating draw game
  let calculateWithDrawNumber = (array) => {
    let _calculateWithDrawNumber = array.reduce((pre, current) => {
      if (current.score.ft[0] === current.score.ft[1]) {
        // console.log(current.team1, current.team2);
        // console.log(current.score.ft[0], current.score.ft[1]);
        // let team1Exist = (pre[current.team1] ?? false) === false ? false : true;
        // let team2Exist = (pre[current.team2] ?? false) === false ? false : true;
        if (
          pre[current.team1] !== undefined &&
          pre[current.team2] !== undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: pre[current.team1] + 1,
            [current.team2]: pre[current.team2] + 1,
          };
          return result;
        } else if (
          pre[current.team1] !== undefined &&
          pre[current.team2] === undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: pre[current.team1] + 1,
            [current.team2]: 1,
          };
          return result;
        } else if (
          pre[current.team2] !== undefined &&
          pre[current.team1] === undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: 1,
            [current.team2]: pre[current.team2] + 1,
          };
          return result;
        } else if (
          pre[current.team1] === undefined &&
          pre[current.team2] === undefined
        ) {
          let result = {
            ...pre,
            [current.team1]: 1,
            [current.team2]: 1,
          };
          return result;
        }
      }
      return pre;
    }, {});

    return _calculateWithDrawNumber;
  };

  let drawGame = calculateWithDrawNumber(playedGame);

  console.log(drawGame);

  let newTable = uniquePlayers.map((club, i) => {
    let obj = { club: club };
    if (playedGame[club] !== undefined) {
      obj = { ...obj, playedGame: playedGame[club] };
    }

    if (winGame[club] !== undefined) {
      obj = { ...obj, winGame: winGame[club] };
    }

    if (lossGame[club] !== undefined) {
      obj = { ...obj, lossGame: lossGame[club] };
    }

    if (drawGame[club] !== undefined) {
      obj = { ...obj, drawGame: drawGame[club] };
    }

    return obj;
  });

  console.log(newTable);

  return <div>Check</div>;
};

export default Check;
